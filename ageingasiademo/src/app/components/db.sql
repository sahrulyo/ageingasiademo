/*

CREATE TABLE UserTypes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_type VARCHAR(255) NOT NULL
);

CREATE TABLE Users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    user_type_id INT,
    FOREIGN KEY (user_type_id) REFERENCES UserTypes(id)
);

CREATE TABLE ServiceTypes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    service_type VARCHAR(255) NOT NULL
);

CREATE TABLE ServiceSubtypes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    service_subtype VARCHAR(255) NOT NULL,
    service_type_id INT,
    FOREIGN KEY (service_type_id) REFERENCES ServiceTypes(id)
);

CREATE TABLE ServiceLevels (
    id INT AUTO_INCREMENT PRIMARY KEY,
    service_level VARCHAR(255) NOT NULL,
    service_subtype_id INT,
    FOREIGN KEY (service_subtype_id) REFERENCES ServiceSubtypes(id)
);

CREATE TABLE OperationalLogic (
    id INT AUTO_INCREMENT PRIMARY KEY,
    description TEXT NOT NULL,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE Accounts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    account_type VARCHAR(255) NOT NULL,
    no_of_account_ids INT,
    sub_trigger_system_update TEXT,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE BusinessMatching (
    id INT AUTO_INCREMENT PRIMARY KEY,
    no_of_matches INT,
    sub_trigger_system_inform_admin TEXT,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE MarketingBranding (
    id INT AUTO_INCREMENT PRIMARY KEY,
    category VARCHAR(255),
    estate_space VARCHAR(255),
    sub_trigger_system_inform_admin TEXT,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE NetworkingEvent (
    id INT AUTO_INCREMENT PRIMARY KEY,
    event_type VARCHAR(255),
    frequency_per_annum INT,
    step_1 TEXT,
    step_2 TEXT,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE MarketIntelligence (
    id INT AUTO_INCREMENT PRIMARY KEY,
    category VARCHAR(255),
    sub_trigger_system_provides_access TEXT,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE Notification (
    id INT AUTO_INCREMENT PRIMARY KEY,
    notification_type VARCHAR(255),
    admin_trigger_system_inform_sub TEXT,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE EventPlanning (
    id INT AUTO_INCREMENT PRIMARY KEY,
    event_type VARCHAR(255),
    admin_creates_event_system_update TEXT,
    service_level_id INT,
    FOREIGN KEY (service_level_id) REFERENCES ServiceLevels(id)
);

CREATE TABLE AdminPortalFeatures (
    id INT AUTO_INCREMENT PRIMARY KEY,
    feature_name VARCHAR(255),
    description TEXT,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

CREATE TABLE MemberPortalFeatures (
    id INT AUTO_INCREMENT PRIMARY KEY,
    feature_name VARCHAR(255),
    description TEXT,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

CREATE TABLE MemberAssist (
    id INT AUTO_INCREMENT PRIMARY KEY,
    member_id INT,
    description TEXT,
    status VARCHAR(255),
    FOREIGN KEY (member_id) REFERENCES Users(id)
);

CREATE TABLE Subscription (
    id INT AUTO_INCREMENT PRIMARY KEY,
    member_id INT,
    membership_tier VARCHAR(255),
    status VARCHAR(255),
    FOREIGN KEY (member_id) REFERENCES Users(id)
);

CREATE TABLE Payments (
    id INT AUTO_INCREMENT PRIMARY KEY,
    member_id INT,
    amount FLOAT,
    payment_date DATE,
    payment_method VARCHAR(255),
    FOREIGN KEY (member_id) REFERENCES Users(id)
);

CREATE TABLE ContentManagement (
    id INT AUTO_INCREMENT PRIMARY KEY,
    member_id INT,
    content_type VARCHAR(255),
    content_details TEXT,
    FOREIGN KEY (member_id) REFERENCES Users(id)
);

CREATE TABLE EventManagement (
    id INT AUTO_INCREMENT PRIMARY KEY,
    event_id INT,
    registration_type VARCHAR(255),
    FOREIGN KEY (event_id) REFERENCES EventPlanning(id)
);

CREATE TABLE MeetingScheduling (
    id INT AUTO_INCREMENT PRIMARY KEY,
    meeting_id INT,
    meeting_date DATETIME,
    attendee_id INT,
    FOREIGN KEY (meeting_id) REFERENCES EventPlanning(id),
    FOREIGN KEY (attendee_id) REFERENCES Users(id)
);

CREATE TABLE ProjectPlanning (
    id INT AUTO_INCREMENT PRIMARY KEY,
    project_name VARCHAR(255),
    start_date DATE,
    end_date DATE,
    status VARCHAR(255),
    admin_id INT,
    FOREIGN KEY (admin_id) REFERENCES Users(id)
);

CREATE TABLE MemberCreditManagement (
    id INT AUTO_INCREMENT PRIMARY KEY,
    member_id INT,
    credits FLOAT,
    FOREIGN KEY (member_id) REFERENCES Users(id)
);

CREATE TABLE TenderAlerts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    tender_id INT,
    member_id INT,
    FOREIGN KEY (tender_id) REFERENCES Notification(id),
    FOREIGN KEY (member_id) REFERENCES Users(id)
);

CREATE TABLE SpeakingSlot (
    id INT AUTO_INCREMENT PRIMARY KEY,
    event_id INT,
    speaker_id INT,
    FOREIGN KEY (event_id) REFERENCES EventPlanning(id),
    FOREIGN KEY (speaker_id) REFERENCES Users(id)
);

-- Tabel untuk mengelola atribut yang berhubungan dengan user
CREATE TABLE UserAttributes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    attribute_name VARCHAR(255),
    attribute_value VARCHAR(255),
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan data penggunaan layanan oleh pengguna
CREATE TABLE ServiceUsage (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    service_id INT,
    usage_date DATE,
    usage_duration INT, -- durasi penggunaan dalam menit atau jam
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (service_id) REFERENCES ServiceLevels(id)
);

-- Tabel untuk menyimpan data evaluasi atau umpan balik dari pengguna terhadap layanan yang digunakan
CREATE TABLE UserFeedback (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    service_id INT,
    feedback_text TEXT,
    feedback_rating INT, -- skala rating dari 1 hingga 5
    feedback_date DATE,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (service_id) REFERENCES ServiceLevels(id)
);

-- Tabel untuk menyimpan informasi tentang transaksi pembayaran
CREATE TABLE Transactions (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    transaction_amount DECIMAL(10, 2), -- jumlah transaksi dengan presisi 10 digit dan 2 digit desimal
    transaction_date DATE,
    transaction_type VARCHAR(255), -- jenis transaksi seperti pembayaran, refund, dll.
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang riwayat login pengguna
CREATE TABLE LoginHistory (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    login_timestamp TIMESTAMP,
    logout_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang interaksi pengguna dengan aplikasi atau situs web
CREATE TABLE UserInteractions (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    interaction_timestamp TIMESTAMP,
    interaction_type VARCHAR(255), -- jenis interaksi seperti klik, navigasi, dll.
    interaction_details TEXT, -- detail atau deskripsi interaksi
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang log aktivitas pengguna
CREATE TABLE UserActivityLog (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    activity_timestamp TIMESTAMP,
    activity_type VARCHAR(255), -- jenis aktivitas seperti login, logout, penggunaan layanan, dll.
    activity_details TEXT, -- detail atau deskripsi aktivitas
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang pengaturan preferensi pengguna
CREATE TABLE UserPreferences (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    preference_name VARCHAR(255),
    preference_value VARCHAR(255),
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang notifikasi yang dikirimkan kepada pengguna
CREATE TABLE UserNotifications (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    notification_type VARCHAR(255), -- jenis notifikasi seperti email, push notification, dll.
    notification_message TEXT, -- pesan notifikasi
    notification_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang preferensi notifikasi pengguna
CREATE TABLE UserNotificationPreferences (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    notification_type VARCHAR(255), -- jenis notifikasi seperti email, push notification, dll.
    is_enabled BOOLEAN, -- status preferensi notifikasi (aktif/tidak aktif)
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang tagihan yang harus dibayar oleh pengguna
CREATE TABLE UserBills (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    bill_amount DECIMAL(10, 2), -- jumlah tagihan dengan presisi 10 digit dan 2 digit desimal
    bill_due_date DATE, -- tanggal jatuh tempo pembayaran
    is_paid BOOLEAN, -- status pembayaran tagihan (lunas/belum lunas)
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang tindak lanjut dari pihak layanan terhadap pengaduan pengguna
CREATE TABLE UserComplaints (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    complaint_text TEXT, -- isi keluhan dari pengguna
    complaint_status VARCHAR(255), -- status penanganan keluhan (dalam proses, selesai, ditolak, dll.)
    resolution_text TEXT, -- resolusi atau tindak lanjut dari pihak layanan terhadap keluhan
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang konten yang disukai oleh pengguna
CREATE TABLE UserLikes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    content_id INT, -- ID konten yang disukai oleh pengguna
    like_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    -- Tambahkan kunci asing ke tabel yang berisi konten (misalnya: Content)
    FOREIGN KEY (content_id) REFERENCES Content(id)
);

-- Tabel untuk menyimpan informasi tentang konten yang dibagikan oleh pengguna
CREATE TABLE UserShares (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    content_id INT, -- ID konten yang dibagikan oleh pengguna
    share_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    -- Tambahkan kunci asing ke tabel yang berisi konten (misalnya: Content)
    FOREIGN KEY (content_id) REFERENCES Content(id)
);

-- Tabel untuk menyimpan informasi tentang konten yang dilihat oleh pengguna
CREATE TABLE UserViews (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    content_id INT, -- ID konten yang dilihat oleh pengguna
    view_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    -- Tambahkan kunci asing ke tabel yang berisi konten (misalnya: Content)
    FOREIGN KEY (content_id) REFERENCES Content(id)
);

-- Tabel untuk menyimpan informasi tentang tag pengguna
CREATE TABLE UserTags (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    tag_name VARCHAR(255), -- Nama tag
    tag_description TEXT, -- Deskripsi tag
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang label atau kategori konten
CREATE TABLE ContentLabels (
    id INT AUTO_INCREMENT PRIMARY KEY,
    content_id INT,
    label_name VARCHAR(255), -- Nama label atau kategori konten
    label_description TEXT, -- Deskripsi label atau kategori konten
    FOREIGN KEY (content_id) REFERENCES Content(id)
);

-- Tabel untuk menyimpan informasi tentang atribut tambahan dari konten
CREATE TABLE ContentAttributes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    content_id INT,
    attribute_name VARCHAR(255), -- Nama atribut
    attribute_value VARCHAR(255), -- Nilai atribut
    FOREIGN KEY (content_id) REFERENCES Content(id)
);

-- Tabel untuk menyimpan informasi tentang komentar yang dibuat oleh pengguna pada konten
CREATE TABLE UserComments (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    content_id INT,
    comment_text TEXT, -- Isi komentar
    comment_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (content_id) REFERENCES Content(id)
);

-- Tabel untuk menyimpan informasi tentang penilaian atau rating konten oleh pengguna
CREATE TABLE UserRatings (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    content_id INT,
    rating_value INT, -- Nilai rating (misalnya: dari 1 hingga 5)
    rating_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (content_id) REFERENCES Content(id)
);

-- Tabel untuk menyimpan informasi tentang pembelian produk atau layanan oleh pengguna
CREATE TABLE UserPurchases (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    product_id INT, -- ID produk yang dibeli oleh pengguna
    purchase_amount DECIMAL(10, 2), -- Jumlah pembelian dengan presisi 10 digit dan 2 digit desimal
    purchase_date DATE,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    -- Tambahkan kunci asing ke tabel yang berisi produk (misalnya: Products)
    FOREIGN KEY (product_id) REFERENCES Products(id)
);

-- Tabel untuk menyimpan informasi tentang riwayat navigasi pengguna
CREATE TABLE UserNavigationHistory (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    page_url VARCHAR(255), -- URL halaman yang dikunjungi oleh pengguna
    visit_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang grup atau komunitas yang diikuti oleh pengguna
CREATE TABLE UserGroups (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    group_id INT, -- ID grup atau komunitas yang diikuti oleh pengguna
    join_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    -- Tambahkan kunci asing ke tabel yang berisi grup atau komunitas (misalnya: Groups)
    FOREIGN KEY (group_id) REFERENCES Groups(id)
);

-- Tabel untuk menyimpan informasi tentang pesan yang dikirimkan oleh pengguna
CREATE TABLE UserMessages (
    id INT AUTO_INCREMENT PRIMARY KEY,
    sender_id INT,
    receiver_id INT,
    message_text TEXT, -- Isi pesan
    message_timestamp TIMESTAMP,
    FOREIGN KEY (sender_id) REFERENCES Users(id),
    FOREIGN KEY (receiver_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang kegiatan atau acara yang dihadiri oleh pengguna
CREATE TABLE UserEvents (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    event_id INT, -- ID acara yang dihadiri oleh pengguna
    attendance_status VARCHAR(255), -- Status kehadiran (hadir, tidak hadir, mungkin hadir, dll.)
    attendance_timestamp TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES Users(id),
    -- Tambahkan kunci asing ke tabel yang berisi acara (misalnya: Events)
    FOREIGN KEY (event_id) REFERENCES Events(id)
);

-- Tabel untuk menyimpan informasi tentang pengiklan
CREATE TABLE Advertisers (
    id INT AUTO_INCREMENT PRIMARY KEY,
    advertiser_name VARCHAR(255), -- Nama pengiklan
    advertiser_contact VARCHAR(255), -- Informasi kontak pengiklan
    advertiser_email VARCHAR(255) -- Email pengiklan
);

-- Tabel untuk menyimpan informasi tentang iklan
CREATE TABLE Advertisements (
    id INT AUTO_INCREMENT PRIMARY KEY,
    advertiser_id INT, -- ID pengiklan
    ad_content TEXT, -- Konten iklan
    ad_placement VARCHAR(255), -- Penempatan iklan
    ad_duration INT, -- Durasi iklan (dalam detik)
    ad_cost DECIMAL(10, 2), -- Biaya iklan dengan presisi 10 digit dan 2 digit desimal
    ad_timestamp TIMESTAMP, -- Waktu iklan dipasang
    FOREIGN KEY (advertiser_id) REFERENCES Advertisers(id)
);

-- Tabel untuk menyimpan informasi tentang statistik iklan
CREATE TABLE AdvertisementStatistics (
    id INT AUTO_INCREMENT PRIMARY KEY,
    ad_id INT, -- ID iklan
    ad_views INT, -- Jumlah tampilan iklan
    ad_clicks INT, -- Jumlah klik iklan
    ad_conversion_rate DECIMAL(5, 2), -- Tingkat konversi iklan (dalam persen)
    ad_statistics_timestamp TIMESTAMP, -- Waktu statistik diambil
    FOREIGN KEY (ad_id) REFERENCES Advertisements(id)
);

-- Tabel untuk menyimpan informasi tentang interaksi pengguna dengan iklan
CREATE TABLE UserAdInteractions (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT, -- ID pengguna
    ad_id INT, -- ID iklan
    interaction_type VARCHAR(255), -- Jenis interaksi (misalnya: tampilan, klik)
    interaction_timestamp TIMESTAMP, -- Waktu interaksi pengguna terjadi
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (ad_id) REFERENCES Advertisements(id)
);

-- Tabel untuk menyimpan informasi tentang produk atau layanan yang ditawarkan oleh bisnis
CREATE TABLE Products (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_name VARCHAR(255), -- Nama produk
    product_description TEXT, -- Deskripsi produk
    product_category VARCHAR(255), -- Kategori produk
    product_price DECIMAL(10, 2) -- Harga produk dengan presisi 10 digit dan 2 digit desimal
);

-- Tabel untuk menyimpan informasi tentang transaksi penjualan
CREATE TABLE Sales (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT, -- ID produk yang terjual
    sale_quantity INT, -- Jumlah produk yang terjual
    sale_amount DECIMAL(10, 2), -- Jumlah pendapatan dari penjualan dengan presisi 10 digit dan 2 digit desimal
    sale_date DATE, -- Tanggal penjualan
    FOREIGN KEY (product_id) REFERENCES Products(id)
);

-- Tabel untuk menyimpan informasi tentang inventaris produk
CREATE TABLE ProductInventory (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT, -- ID produk dalam inventaris
    inventory_quantity INT, -- Jumlah produk dalam inventaris
    inventory_location VARCHAR(255), -- Lokasi inventaris produk
    last_inventory_update TIMESTAMP, -- Waktu terakhir inventaris diperbarui
    FOREIGN KEY (product_id) REFERENCES Products(id)
);

-- Tabel untuk menyimpan informasi tentang ulasan produk oleh pengguna
CREATE TABLE ProductReviews (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT, -- ID produk yang diulas oleh pengguna
    user_id INT, -- ID pengguna yang memberikan ulasan
    review_text TEXT, -- Isi ulasan produk
    review_rating INT, -- Nilai ulasan produk (misalnya: dari 1 hingga 5)
    review_timestamp TIMESTAMP, -- Waktu ulasan dibuat
    FOREIGN KEY (product_id) REFERENCES Products(id),
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang wishlist pengguna
CREATE TABLE UserWishlist (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT, -- ID pengguna
    product_id INT, -- ID produk dalam wishlist pengguna
    wishlist_timestamp TIMESTAMP, -- Waktu produk ditambahkan ke wishlist
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (product_id) REFERENCES Products(id)
);

-- Tabel untuk menyimpan informasi tentang riwayat transaksi pengguna
CREATE TABLE UserTransactionHistory (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT, -- ID pengguna
    transaction_type VARCHAR(255), -- Jenis transaksi (misalnya: pembelian, pengembalian)
    transaction_amount DECIMAL(10, 2), -- Jumlah transaksi dengan presisi 10 digit dan 2 digit desimal
    transaction_date DATE, -- Tanggal transaksi
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang pengembalian produk oleh pengguna
CREATE TABLE ProductReturns (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT, -- ID produk yang dikembalikan oleh pengguna
    user_id INT, -- ID pengguna yang mengembalikan produk
    return_reason TEXT, -- Alasan pengembalian produk
    return_date DATE, -- Tanggal pengembalian produk
    FOREIGN KEY (product_id) REFERENCES Products(id),
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang pemesanan produk oleh pengguna
CREATE TABLE ProductOrders (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT, -- ID pengguna yang memesan produk
    product_id INT, -- ID produk yang dipesan oleh pengguna
    order_quantity INT, -- Jumlah produk yang dipesan
    order_date DATE, -- Tanggal pemesanan produk
    order_status VARCHAR(255), -- Status pemesanan (misalnya: diproses, dikirim, diterima)
    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (product_id) REFERENCES Products(id)
);

-- Tabel untuk menyimpan informasi tentang alamat pengiriman pengguna
CREATE TABLE UserShippingAddresses (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT, -- ID pengguna
    address_line1 VARCHAR(255), -- Alamat baris 1
    address_line2 VARCHAR(255), -- Alamat baris 2 (opsional)
    city VARCHAR(255), -- Kota
    state VARCHAR(255), -- Provinsi atau negara bagian
    country VARCHAR(255), -- Negara
    postal_code VARCHAR(20), -- Kode pos
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang metode pembayaran yang digunakan oleh pengguna
CREATE TABLE UserPaymentMethods (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT, -- ID pengguna
    payment_method_type VARCHAR(255), -- Jenis metode pembayaran (misalnya: kartu kredit, PayPal)
    payment_method_details TEXT, -- Detail metode pembayaran (misalnya: nomor kartu kredit, alamat PayPal)
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

-- Tabel untuk menyimpan informasi tentang diskon yang ditawarkan pada produk
CREATE TABLE ProductDiscounts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT, -- ID produk yang memiliki diskon
    discount_percentage DECIMAL(5, 2), -- Persentase diskon (dalam persen)
    discount_description TEXT, -- Deskripsi diskon
    start_date DATE, -- Tanggal mulai diskon
    end_date DATE, -- Tanggal berakhirnya diskon
    FOREIGN KEY (product_id) REFERENCES Products(id)
);

-- Tabel untuk menyimpan informasi tentang pengaturan situs web atau aplikasi
CREATE TABLE SiteSettings (
    id INT AUTO_INCREMENT PRIMARY KEY,
    setting_name VARCHAR(255), -- Nama pengaturan
    setting_value TEXT -- Nilai pengaturan
);

-- Tabel untuk menyimpan informasi tentang log aktivitas pengguna
CREATE TABLE UserActivityLogs (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT, -- ID pengguna
    activity_type VARCHAR(255), -- Jenis aktivitas (misalnya: masuk, keluar, memperbarui profil)
    activity_timestamp TIMESTAMP, -- Waktu aktivitas terjadi
    FOREIGN KEY (user_id) REFERENCES Users(id)
);

*/

