// File: pages/about.js atau pages/about.tsx

import Link from 'next/link';

const AboutPage = () => {
  return (
    <div>
      <h1>About Us</h1>
      <p>This is the about page of our website.</p>
      <Link href="/">
        <a>Go back to home</a>
      </Link>
    </div>
  );
};

export default AboutPage;
